//
//  TabBarViewController.swift
//  SmartLabClinic
//
//  Created by minhnh on 10/5/16.
//  Copyright © 2016 smartlab. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    let storyboardNames = [Constant.DashboardVC_Storyboard, Constant.SettingVC_Storyboard]
    let tabVCNames = [Constant.DashboardNav_StoryboardID, Constant.SettingNav_StoryboardID]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initViewControllersForEachTab()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Privates
    private func initViewControllersForEachTab() {
        var vcsArray = [UIViewController]()
        
        for i in 0 ..< storyboardNames.count {
            let storyboard = UIStoryboard(name: storyboardNames[i], bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: tabVCNames[i])
            
            vcsArray.append(vc)
        }
        
        viewControllers = vcsArray
    }
}
