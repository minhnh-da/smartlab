//
//  VerifyCodeViewController.swift
//  SmartLabClinic
//
//  Created by minhnh on 10/5/16.
//  Copyright © 2016 smartlab. All rights reserved.
//

import UIKit

class VerifyCodeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Action
    @IBAction func verifyBtnClicked(_ sender: UIButton) {
        let tabBarVCStoryboard = UIStoryboard(name: Constant.TabBarVC_Storyboard, bundle: nil)
        let tbvc = tabBarVCStoryboard.instantiateViewController(withIdentifier: Constant.UITabBar_StoryboardID)
        present(tbvc, animated: true, completion: nil)
    }
    
}
