//
//  Constant.swift
//  SmartLabClinic
//
//  Created by minhnh on 10/5/16.
//  Copyright © 2016 smartlab. All rights reserved.
//

import Foundation
class Constant : NSObject {
    //MARK: - Storyboard name
    static let Login_Storyboard = "Login"
    static let TabBarVC_Storyboard = "TabBarVC"
    static let DashboardVC_Storyboard = "Dashboard"
    static let SettingVC_Storyboard = "Setting"
    //MARK: - StoryboardID
    static let UITabBar_StoryboardID = "TabBarViewController"
    static let DashboardNav_StoryboardID = "DashboardNav"
    static let DashboardVC_StoryboardID = "DashboardVC"
    static let SettingNav_StoryboardID = "SettingNav"
    static let SettingVC_StoryboardID = "SettingVC"
}
